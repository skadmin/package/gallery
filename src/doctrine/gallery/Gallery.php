<?php

declare(strict_types=1);

namespace Skadmin\Gallery\Doctrine\Gallery;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use SkadminUtils\DoctrineTraits\Entity;

use function array_merge;
use function array_slice;
use function shuffle;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Gallery
{
    use Entity\Id;
    use Entity\WebalizeName;
    use Entity\Content;
    use Entity\IsActive;
    use Entity\ImagePreview;
    use Entity\Sequence;
    use Entity\AdditionalSettings;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'galleries')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ?Gallery $gallery = null;

    /** @var ArrayCollection|Collection|Gallery[] */
    #[ORM\OneToMany(targetEntity: self::class, mappedBy: 'gallery')]
    #[ORM\OrderBy(['sequence' => 'ASC'])]
    private ArrayCollection|Collection|array $galleries;

    /** @var ArrayCollection|Collection|Photo[] */
    #[ORM\OneToMany(targetEntity: Photo::class, mappedBy: 'gallery')]
    #[ORM\OrderBy(['sequence' => 'ASC'])]
    private ArrayCollection|Collection|array $photos;

    public function __construct()
    {
        $this->galleries = new ArrayCollection();
        $this->photos    = new ArrayCollection();
    }

    /**
     * @param array<mixed> $additionalSettings
     */
    public function update(?Gallery $gallery, string $name, bool $isActive, string $content, ?string $imagePreview, array $additionalSettings): void
    {
        $this->gallery            = $gallery;
        $this->name               = $name;
        $this->isActive           = $isActive;
        $this->content            = $content;
        $this->additionalSettings = $additionalSettings;

        if ($imagePreview === null || $imagePreview === '') {
            return;
        }

        $this->imagePreview = $imagePreview;
    }

    public function getGallery(): ?Gallery
    {
        return $this->gallery;
    }

    /**
     * @return Gallery[]
     */
    public function getParentGalleries(bool $includeSelf = false): array
    {
        $galleries = [];

        if ($includeSelf) {
            $galleries[] = $this;
        }

        $gallery = $this->getGallery();
        while ($gallery !== null) {
            $galleries[] = $gallery;
            $gallery     = $gallery->getGallery();
        }

        return $galleries;
    }

    /**
     * @return ArrayCollection|Collection|Gallery[]
     */
    public function getGalleries(): ArrayCollection|Collection|array
    {
        return $this->galleries;
    }

    /**
     * @return Gallery[]
     */
    public function getAllSubGalleries(): array
    {
        $galleries = [];

        foreach ($this->galleries as $gallery) {
            $galleries[] = $gallery;
            $galleries   = array_merge($galleries, $gallery->getAllSubGalleries());
        }

        return $galleries;
    }

    /**
     * @return ArrayCollection<Photo>|Collection|array<Photo>
     */
    public function getPhotos(bool $onlyActive = false, ?int $limit = null, bool $random = false): ArrayCollection|Collection|array
    {
        $criteria = Criteria::create();

        if ($onlyActive) {
            $criteria->where(Criteria::expr()->eq('isActive', true));
        }

        if ($random) {
            $photos = $this->photos->matching($criteria)->toArray();
            shuffle($photos);

            return array_slice($photos, 0, $limit);
        }

        $criteria->setMaxResults($limit);

        return $this->photos->matching($criteria);
    }
}
