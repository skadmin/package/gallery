<?php

declare(strict_types=1);

namespace Skadmin\Gallery\Doctrine\Gallery;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;
use SkadminUtils\Gallery\Factory\GallerySettingsFactory;

final class GalleryFacade extends Facade
{
    private GallerySettingsFactory $gallerySettings;

    use Facade\Webalize;
    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em, GallerySettingsFactory $gallerySettings)
    {
        parent::__construct($em);
        $this->table = Gallery::class;

        $this->gallerySettings = $gallerySettings;
    }

    /**
     * @param array<mixed> $additionalSettings
     */
    public function create(?Gallery $parentGallery, string $name, bool $isActive, string $content, ?string $imagePreview, array $additionalSettings): Gallery
    {
        return $this->update(null, $parentGallery, $name, $isActive, $content, $imagePreview, $additionalSettings);
    }

    /**
     * @param array<mixed> $additionalSettings
     */
    public function update(?int $id, ?Gallery $parentGallery, string $name, bool $isActive, string $content, ?string $imagePreview, array $additionalSettings): Gallery
    {
        $gallery = $this->get($id);
        $gallery->update($parentGallery, $name, $isActive, $content, $imagePreview, $additionalSettings);

        if (! $gallery->isLoaded()) {
            $gallery->setWebalize($this->getValidWebalize($name));

            if ($this->gallerySettings->isPrependGallery() && $parentGallery instanceof Gallery) {
                foreach ($parentGallery->getGalleries() as $galleryGallery) {
                    $galleryGallery->updateSequence($galleryGallery->getSequence() + 1);
                }

                $gallery->setSequence(1);
            } else {
                $gallery->setSequence($this->getValidSequence($parentGallery instanceof Gallery ? $parentGallery->getId() : null));
            }
        }

        $this->em->persist($gallery);
        $this->em->flush();

        return $gallery;
    }

    /**
     * @param array<int, int> $newOrder
     */
    public function updateSequence(array $newOrder): void
    {
        foreach ($newOrder as $galleryId => $sequence) {
            $gallery = $this->get($galleryId);

            if (! $gallery->isLoaded()) {
                continue;
            }

            $gallery->updateSequence($sequence);
            $this->em->persist($gallery);
        }

        $this->em->flush();
    }

    public function get(?int $id = null): Gallery
    {
        if ($id === null) {
            return new Gallery();
        }

        $gallery = parent::get($id);

        if ($gallery === null) {
            return new Gallery();
        }

        return $gallery;
    }

    /**
     * @return Gallery[]
     */
    public function getAll(bool $onlyActive = false): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    /**
     * @return Gallery[]
     */
    public function getAllRoot(bool $onlyActive = false): array
    {
        $criteria = ['gallery' => null];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    /**
     * @return Gallery[]
     */
    public function getForGallery(?Gallery $gallery, bool $onlyActive = false): array
    {
        if ($gallery === null || ! $gallery->isLoaded()) {
            $gallery = null;
        }

        $criteria = ['gallery' => $gallery];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    public function findByWebalize(string $webalize): ?Gallery
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    public function getModel(?string $table = null): QueryBuilder
    {
        return parent::getModel($table)
            ->orderBy('a.sequence');
    }

    public function remove(Gallery $gallery): void
    {
        $this->em->remove($gallery);
        $this->em->flush();
    }

    public function getValidSequence(?int $galleryId): int
    {
        $criteria = Criteria::create();
        if ($galleryId === null) {
            $criteria->where(Criteria::expr()->isNull('a.gallery'));
        } else {
            $criteria->where(Criteria::expr()->eq('a.gallery', $galleryId));
        }

        $sequence = $this->getModel()
            ->select('count(a.id)')
            ->addCriteria($criteria)
            ->getQuery()
            ->getSingleScalarResult();

        return intval($sequence) + 1;
    }
}
