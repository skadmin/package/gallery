<?php

declare(strict_types=1);

namespace Skadmin\Gallery\Doctrine\Gallery;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;
use SkadminUtils\Gallery\Factory\GallerySettingsFactory;
use SkadminUtils\Utils\Parser\YouTubeParser;

use function assert;
use function count;
use function explode;
use function implode;

final class PhotoFacade extends Facade
{
    private GallerySettingsFactory $gallerySettings;

    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em, GallerySettingsFactory $gallerySettings)
    {
        parent::__construct($em);
        $this->table = Photo::class;

        $this->gallerySettings = $gallerySettings;
    }

    public function create(Gallery $gallery, string $name, string $title, bool $isActive, string $video, ?string $imagePreview): Photo
    {
        $eName = explode('.', $name);
        if (count($eName) > 1) {
            unset($eName[count($eName) - 1]);
            $name = implode('.', $eName);
        }

        return $this->update(null, $gallery, $name, $title, $isActive, $video, $imagePreview);
    }

    public function update(?int $id, Gallery $gallery, string $name, string $title, bool $isActive, string $video, ?string $imagePreview): Photo
    {
        $photo = $this->get($id);
        $photo->update($gallery, $name, $title, $isActive, YouTubeParser::parseYouTubeTokenFromUri($video), $imagePreview);

        if (! $photo->isLoaded()) {
            if ($this->gallerySettings->isPrependPhoto()) {
                foreach ($gallery->getPhotos() as $galleryPhoto) {
                    $galleryPhoto->updateSequence($galleryPhoto->getSequence() + 1);
                }

                $photo->setSequence(1);
            } else {
                $photo->setSequence($this->getValidSequence($gallery->getId()));
            }
        }

        $this->em->persist($photo);
        $this->em->flush();

        return $photo;
    }

    /**
     * @param array<int, int> $newOrder
     */
    public function updateSequence(array $newOrder): void
    {
        foreach ($newOrder as $photoId => $sequence) {
            $photo = $this->get($photoId);

            if (! $photo->isLoaded()) {
                continue;
            }

            $photo->updateSequence($sequence);
            $this->em->persist($photo);
        }

        $this->em->flush();
    }

    public function get(?int $id = null): Photo
    {
        if ($id === null) {
            return new Photo();
        }

        $photo = parent::get($id);

        if ($photo === null) {
            return new Photo();
        }

        return $photo;
    }

    /**
     * @return Photo[]
     */
    public function getAll(bool $onlyActive = false): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    public function getModelForGallery(Gallery $gallery): QueryBuilder
    {
        $repository = $this->em
            ->getRepository($this->table);
        assert($repository instanceof EntityRepository);

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('gallery', $gallery));

        return $repository->createQueryBuilder('a')
            ->orderBy('a.sequence')
            ->addCriteria($criteria);
    }

    public function remove(Photo $photo): void
    {
        $this->em->remove($photo);
        $this->em->flush();
    }

    public function getValidSequence(int $galleryId): int
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('a.gallery', $galleryId));

        $sequence = $this->getModel()
            ->select('count(a.id)')
            ->addCriteria($criteria)
            ->getQuery()
            ->getSingleScalarResult();

        return intval($sequence) + 1;
    }
}
