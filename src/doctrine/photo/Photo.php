<?php

declare(strict_types=1);

namespace Skadmin\Gallery\Doctrine\Gallery;

use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\Strings;
use SkadminUtils\DoctrineTraits\Entity;

use function trim;

#[ORM\Table(name: 'gallery_photo')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Photo
{
    use Entity\Id;
    use Entity\Name;
    use Entity\IsActive;
    use Entity\ImagePreview;
    use Entity\Title;
    use Entity\Sequence;

    #[ORM\Column(nullable: true)]
    private ?string $video = null;

    #[ORM\ManyToOne(targetEntity: Gallery::class, inversedBy: 'photos')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Gallery $gallery;

    public function update(Gallery $gallery, string $name, string $title, bool $isActive, string $video, ?string $imagePreview): void
    {
        $this->gallery  = $gallery;
        $this->name     = $name;
        $this->title    = $title;
        $this->isActive = $isActive;
        $this->video    = $video;

        if ($imagePreview === null || $imagePreview === '') {
            return;
        }

        $this->imagePreview = $imagePreview;
    }

    public function getTitle(bool $force = false): string
    {
        if ($force) {
            return $this->title;
        }

        return $this->title !== '' ? $this->title : $this->getName();
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    public function isYoutubeToken(): bool
    {
        return $this->isVideo() && Strings::length($this->video) < 15;
    }

    public function isVideo(): bool
    {
        return $this->video !== null && trim($this->video) !== '';
    }

    public function getGallery(): Gallery
    {
        return $this->gallery;
    }
}
