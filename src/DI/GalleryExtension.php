<?php

declare(strict_types=1);

namespace SkadminUtils\Gallery\DI;

use Nette\DI\CompilerExtension;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use SkadminUtils\Gallery\Factory\GallerySettingsFactory;

class GalleryExtension extends CompilerExtension
{
    public function getConfigSchema(): Schema
    {
        return Expect::structure([
            'allowVideo' => Expect::bool(false),
            'prependGallery' => Expect::bool(false),
            'prependPhoto' => Expect::bool(false),
        ]);
    }

    public function loadConfiguration(): void
    {
        parent::loadConfiguration();

        $builder = $this->getContainerBuilder();
        $builder->addDefinition($this->prefix('gallerySettingsFactory'))
            ->setClass(GallerySettingsFactory::class)
            ->setArguments([
                $this->config->allowVideo,
                $this->config->prependGallery,
                $this->config->prependPhoto,
            ]);
    }
}
