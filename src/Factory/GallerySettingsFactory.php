<?php

declare(strict_types=1);

namespace SkadminUtils\Gallery\Factory;

class GallerySettingsFactory
{
    private bool $allowVideo;
    private bool $prependGallery;
    private bool $prependPhoto;

    public function __construct(bool $allowVideo, bool $prependGallery, bool $prependPhoto)
    {
        $this->allowVideo     = $allowVideo;
        $this->prependGallery = $prependGallery;
        $this->prependPhoto   = $prependPhoto;
    }

    public function isAllowVideo(): bool
    {
        return $this->allowVideo;
    }

    public function isPrependGallery(): bool
    {
        return $this->prependGallery;
    }

    public function isPrependPhoto(): bool
    {
        return $this->prependPhoto;
    }
}
