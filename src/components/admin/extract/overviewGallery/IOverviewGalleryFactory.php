<?php

declare(strict_types=1);

namespace Skadmin\Gallery\Components\Admin;

interface IOverviewGalleryFactory
{
    public function create(?int $id = null): OverviewGallery;
}
