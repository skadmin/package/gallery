<?php

declare(strict_types=1);

namespace Skadmin\Gallery\Components\Admin;

use App\Components\Form\FormDropzone;
use App\Components\Form\IFormDropzoneFactory;
use App\Components\Grid\TemplateControl;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Http\FileUpload;
use Nette\Security\User;
use Nette\Utils\Strings;
use Skadmin\Gallery\BaseControl;
use Skadmin\Gallery\Doctrine\Gallery\Gallery;
use Skadmin\Gallery\Doctrine\Gallery\GalleryFacade;
use Skadmin\Gallery\Doctrine\Gallery\Photo;
use Skadmin\Gallery\Doctrine\Gallery\PhotoFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\Gallery\Factory\GallerySettingsFactory;
use SkadminUtils\ImageStorage\ImageStorage;
use SkadminUtils\Utils\Parser\YouTubeParser;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function file_get_contents;
use function json_decode;
use function sprintf;

class OverviewGallery extends TemplateControl
{
    use APackageControl;

    private GallerySettingsFactory $gallerySettings;
    private GalleryFacade          $facade;
    private PhotoFacade            $facadePhoto;
    private ImageStorage           $imageStorage;
    private LoaderFactory          $webLoader;
    private User                   $loggedUser;
    private Gallery                $gallery;
    private EditGallery            $editGallery;
    private EditPhoto              $editPhoto;
    private FormDropzone           $formDropzoneFactory;

    public function __construct(?int $id, GallerySettingsFactory $gallerySettings, GalleryFacade $facade, PhotoFacade $facadePhoto, Translator $translator, ImageStorage $imageStorage, LoaderFactory $webLoader, User $user, IEditGalleryFactory $iEditGalleryFactory, IEditPhotoFactory $iEditPhotoFactory, IFormDropzoneFactory $iFormDropzoneFactory)
    {
        parent::__construct($translator);

        $this->gallerySettings = $gallerySettings;
        $this->facade          = $facade;
        $this->facadePhoto     = $facadePhoto;
        $this->imageStorage    = $imageStorage;
        $this->webLoader       = $webLoader;
        $this->loggedUser      = $user;

        $this->gallery             = $this->facade->get($id);
        $this->editGallery         = $iEditGalleryFactory->create();
        $this->editPhoto           = $iEditPhotoFactory->create();
        $this->formDropzoneFactory = $iFormDropzoneFactory->create('photos');
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->loggedUser->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overviewGallery.latte');

        $template->imageStorage = $this->imageStorage;
        $template->linkRoot     = $this->getPresenter()
            ->link('Component:default', [
                'package' => new BaseControl(),
                'render'  => 'overviewGallery',
            ]);

        $template->galleries       = $this->facade->getForGallery($this->gallery);
        $template->currentGallery  = $this->gallery;
        $template->gallerySettings = $this->gallerySettings;

        $template->render();
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->gallery->isLoaded()) {
            return new SimpleTranslation('gallery.overview.title - %s', $this->gallery->getName());
        }

        return 'gallery.overview.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        $csss = $this->editGallery->getCss();
        foreach ($this->editPhoto->getCss() as $css) {
            $csss[] = $css;
        }

        foreach ($this->formDropzoneFactory->getCss() as $css) {
            $csss[] = $css;
        }

        $csss[] = $this->webLoader->createCssLoader('jQueryUi');

        return $csss;
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        $jss = $this->editGallery->getJs();
        foreach ($this->editPhoto->getJs() as $js) {
            $jss[] = $js;
        }

        foreach ($this->formDropzoneFactory->getJs() as $js) {
            $jss[] = $js;
        }

        $jss[] = $this->webLoader->createJavaScriptLoader('jQueryUi');

        return $jss;
    }

    public function handleUpdateSequenceGallery(?string $jsonOrder = null): void
    {
        if ($jsonOrder === null) {
            return;
        }

        $this->facade->updateSequence(json_decode($jsonOrder, true));
        $this->onFlashmessage('form.gallery.overview-gallery.flash.success.update-gallery-sequence', Flash::SUCCESS);
    }

    public function handleUpdateSequencePhoto(?string $jsonOrder = null): void
    {
        if ($jsonOrder === null) {
            return;
        }

        $this->facadePhoto->updateSequence(json_decode($jsonOrder, true));
        $this->onFlashmessage('form.gallery.overview-gallery.flash.success.update-photo-sequence', Flash::SUCCESS);
    }

    protected function createComponentFormDropzone(): FormDropzone
    {
        $this->formDropzoneFactory->onFileUpload[] = [$this, 'formDropzoneOnFileUpload']; //@phpstan-ignore-line
        $this->formDropzoneFactory->onRedraw[]     = [$this, 'formDropzoneOnRedraw'];

        return $this->formDropzoneFactory;
    }

    public function formDropzoneOnFileUpload(FileUpload $file): void
    {
        if (! $file->isOk() || ! $file->isImage()) {
            return;
        }

        $identifier = $this->imageStorage->saveUpload($file, BaseControl::DIR_IMAGE_PHOTO)->identifier;
        $this->facadePhoto->create($this->gallery, $file->getName(), '', true, '', $identifier);
    }

    public function formDropzoneOnRedraw(): void
    {
        $this->redrawControl('snipCountPhotos');
        $this->redrawControl('snipPhotos');
    }

    public function handleRemoveGallery(int $id): void
    {
        $gallery = $this->facade->get($id);

        if (! $gallery->isLoaded()) {
            return;
        }

        $this->removeGallery($gallery);
    }

    private function removeGallery(Gallery $gallery): void
    {
        foreach ($gallery->getGalleries() as $subGallery) {
            $this->removeGallery($subGallery);
        }

        foreach ($gallery->getPhotos() as $photo) {
            $this->removePhoto($photo, false);
        }

        if ($gallery->getImagePreview() !== null) {
            $this->imageStorage->delete($gallery->getImagePreview());
        }

        $this->facade->remove($gallery);

        $this->redrawControl('snipCountGalleries');
        $this->redrawControl('snipGalleries');
    }

    public function handleRemovePhoto(int $id): void
    {
        $photo = $this->facadePhoto->get($id);

        if (! $photo->isLoaded()) {
            return;
        }

        $this->removePhoto($photo);
    }

    private function removePhoto(Photo $photo, bool $redrawControl = true): void
    {
        if ($photo->getImagePreview() !== null) {
            $this->imageStorage->delete($photo->getImagePreview());
        }

        $this->facadePhoto->remove($photo);

        if (! $redrawControl) {
            return;
        }

        $this->redrawControl('snipCountPhotos');
        $this->redrawControl('snipPhotos');
    }

    public function handleAddVideo(?string $video = null): void
    {
        $token     = YouTubeParser::parseYouTubeTokenFromUri($video);
        $videoName = YouTubeParser::getYouTubeTitle($token);

        $videoPreviewUrl = sprintf('https://img.youtube.com/vi/%s/hqdefault.jpg', $token);
        $videoPath       = sprintf('%s.jpg', Strings::webalize($videoName));
        $identifier      = $this->imageStorage->saveContent(file_get_contents($videoPreviewUrl), $videoPath, BaseControl::DIR_IMAGE_PHOTO)->identifier;

        $this->facadePhoto->create($this->gallery, $videoName, '', true, $token, $identifier);

        $this->redrawControl('snipCountPhotos');
        $this->redrawControl('snipPhotos');
    }
}
