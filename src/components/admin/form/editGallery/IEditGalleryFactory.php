<?php

declare(strict_types=1);

namespace Skadmin\Gallery\Components\Admin;

interface IEditGalleryFactory
{
    public function create(?int $id = null): EditGallery;
}
