<?php

declare(strict_types=1);

namespace Skadmin\Gallery\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Http\Request;
use Nette\Security\User;
use Nette\Utils\ArrayHash;
use Skadmin\Gallery\BaseControl;
use Skadmin\Gallery\Doctrine\Gallery\Gallery;
use Skadmin\Gallery\Doctrine\Gallery\GalleryFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function intval;

class EditGallery extends FormWithUserControl
{
    use APackageControl;

    private GalleryFacade $facade;
    private ImageStorage  $imageStorage;
    private LoaderFactory $webLoader;
    private Gallery       $gallery;
    private ?int          $parentGalleryId = null;

    public function __construct(?int $id, Request $request, GalleryFacade $facade, Translator $translator, ImageStorage $imageStorage, LoaderFactory $webLoader, User $user)
    {
        parent::__construct($translator, $user);

        $this->parentGalleryId = intval($request->getQuery('galleryId'));

        $this->facade       = $facade;
        $this->imageStorage = $imageStorage;
        $this->webLoader    = $webLoader;
        $this->loggedUser   = $user;

        $this->gallery = $this->facade->get($id);
        $this->isModal = true;
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->gallery->isLoaded()) {
            return new SimpleTranslation('form.gallery.edit-gallery.title - %s', $this->gallery->getName());
        }

        return 'form.gallery.edit-gallery.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('colorPicker'),
            $this->webLoader->createCssLoader('customFileInput'),
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('colorPicker'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('skadminForms'),
        ];
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editGallery.latte');

        $template->gallery = $this->gallery;
        $template->drawBox = $this->drawBox;
        $template->isModal = $this->isModal;

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // DATA
        $dataGallery = $this->facade->getPairs('id', 'name');
        if ($this->gallery->isLoaded()) {
            // remove self
            unset($dataGallery[$this->gallery->getId()]);

            // remove subgalleries
            foreach ($this->gallery->getAllSubGalleries() as $removeGallery) {
                unset($dataGallery[$removeGallery->getId()]);
            }
        }

        // INPUT
        $form->addText('name', 'form.gallery.edit-gallery.name')
            ->setRequired('form.gallery.edit-gallery.name.req');
        $form->addTextArea('content', 'form.gallery.edit-gallery.content');
        $inputGallery = $form->addSelect('gallery', 'form.gallery.edit-gallery.gallery', $dataGallery)
            ->setPrompt('--')
            ->setTranslator(null);

        $form->addImageWithRFM('imagePreview', 'form.gallery.edit-gallery.image-preview');

        $form->addCheckbox('isActive', 'form.gallery.edit-gallery.is-active')
            ->setDefaultValue(true);

        $form->addInlineArray('additionalSettings', 'form.gallery.edit-gallery.additional-settings');

        // BUTTON
        $form->addSubmit('send', 'form.gallery.edit-gallery.send');
        $form->addSubmit('sendBack', 'form.gallery.edit-gallery.send-back');

        // @phpstan-ignore-next-line
        $form->addSubmit('back', 'form.gallery.edit-gallery.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        if (isset($dataGallery[$this->parentGalleryId])) {
            $inputGallery->setDefaultValue($this->parentGalleryId);
        }

        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->imagePreview, BaseControl::DIR_IMAGE);

        // GALLERY
        $parentGallery = $this->facade->get($values->gallery);

        if ($this->gallery->isLoaded()) {
            if ($identifier !== null && $this->gallery->getImagePreview() !== null) {
                $this->imageStorage->delete($this->gallery->getImagePreview());
            }

            $gallery = $this->facade->update(
                $this->gallery->getId(),
                $parentGallery->isLoaded() ? $parentGallery : null,
                $values->name,
                $values->isActive,
                $values->content,
                $identifier,
                $values->additionalSettings
            );
            $this->onFlashmessage('form.gallery.edit-gallery.flash.success.update', Flash::SUCCESS);
        } else {
            $gallery = $this->facade->create(
                $parentGallery->isLoaded() ? $parentGallery : null,
                $values->name,
                $values->isActive,
                $values->content,
                $identifier,
                $values->additionalSettings
            );
            $this->onFlashmessage('form.gallery.edit-gallery.flash.success.create', Flash::SUCCESS);
        }

        //if ($form->isSubmitted()->name === 'sendBack') {
        $this->processOnBack($gallery->getId());
        //}

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-gallery',
            'id'      => $gallery->getId(),
        ]);
    }

    public function processOnBack(?int $galleryId = null): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-gallery',
            'id'      => $galleryId,
        ]);
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->gallery->isLoaded()) {
            return [];
        }

        return [
            'gallery'            => $this->gallery->getGallery() !== null ? $this->gallery->getGallery()->getId() : null,
            'name'               => $this->gallery->getName(),
            'content'            => $this->gallery->getContent(),
            'isActive'           => $this->gallery->isActive(),
            'additionalSettings' => $this->gallery->getAdditionalSettings(),
        ];
    }
}
