<?php

declare(strict_types=1);

namespace Skadmin\Gallery\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Http\Request;
use Nette\Security\User;
use Nette\Utils\ArrayHash;
use Skadmin\Gallery\BaseControl;
use Skadmin\Gallery\Doctrine\Gallery\GalleryFacade;
use Skadmin\Gallery\Doctrine\Gallery\Photo;
use Skadmin\Gallery\Doctrine\Gallery\PhotoFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\Gallery\Factory\GallerySettingsFactory;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function intval;
use function sprintf;

class EditPhoto extends FormWithUserControl
{
    use APackageControl;

    private GallerySettingsFactory $gallerySettings;
    private PhotoFacade            $facade;
    private GalleryFacade          $facadeGallery;
    private ImageStorage           $imageStorage;
    private LoaderFactory          $webLoader;
    private Photo                  $photo;
    private ?int                   $parentGalleryId = null;

    public function __construct(?int $id, GallerySettingsFactory $gallerySettings, Request $request, PhotoFacade $facade, GalleryFacade $facadeGallery, Translator $translator, ImageStorage $imageStorage, LoaderFactory $webLoader, User $user)
    {
        parent::__construct($translator, $user);

        $this->parentGalleryId = intval($request->getQuery('galleryId'));

        $this->gallerySettings = $gallerySettings;
        $this->facade          = $facade;
        $this->facadeGallery   = $facadeGallery;
        $this->imageStorage    = $imageStorage;
        $this->webLoader       = $webLoader;
        $this->loggedUser      = $user;

        $this->photo   = $this->facade->get($id);
        $this->isModal = true;
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->photo->isLoaded()) {
            return new SimpleTranslation('form.gallery.edit-photo.title - %s', $this->photo->getName());
        }

        return 'form.gallery.edit-photo.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('customFileInput'),
        ];
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editPhoto.latte');

        $template->photo           = $this->photo;
        $template->drawBox         = $this->drawBox;
        $template->isModal         = $this->isModal;
        $template->gallerySettings = $this->gallerySettings;

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // DATA
        $dataGallery = $this->facadeGallery->getPairs('id', 'name');

        // INPUT
        $form->addText('name', 'form.gallery.edit-photo.name')
            ->setRequired('form.gallery.edit-photo.name.req');
        $form->addText('title', 'form.gallery.edit-photo.title');
        $inputGallery = $form->addSelect('gallery', 'form.gallery.edit-photo.gallery', $dataGallery)
            //->setPrompt('--')
            ->setTranslator(null);

        $form->addImageWithRFM('imagePreview', 'form.gallery.edit-photo.image-preview');

        $form->addCheckbox('isActive', 'form.gallery.edit-photo.is-active')
            ->setDefaultValue(true);

        $form->addUrl('video', 'form.gallery.edit-photo.video', ['youtube.com', 'youtu.be', 'youtube-nocookie.com']);

        // BUTTON
        $form->addSubmit('send', 'form.gallery.edit-photo.send');
        $form->addSubmit('sendBack', 'form.gallery.edit-photo.send-back');

        // @phpstan-ignore-next-line
        $form->addSubmit('back', 'form.gallery.edit-photo.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        if (isset($dataGallery[$this->parentGalleryId])) {
            $inputGallery->setDefaultValue($this->parentGalleryId);
        }

        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->imagePreview, BaseControl::DIR_IMAGE_PHOTO);

        // GALLERY
        $parentGallery = $this->facadeGallery->get($values->gallery);

        if ($this->photo->isLoaded()) {
            if ($identifier !== null && $this->photo->getImagePreview() !== null) {
                $this->imageStorage->delete($this->photo->getImagePreview());
            }

            $photo = $this->facade->update(
                $this->photo->getId(),
                $parentGallery,
                $values->name,
                $values->title,
                $values->isActive,
                $values->video,
                $identifier
            );
            $this->onFlashmessage('form.gallery.edit-photo.flash.success.update', Flash::SUCCESS);
        } else {
            $photo = $this->facade->create(
                $parentGallery,
                $values->name,
                $values->title,
                $values->isActive,
                $values->video,
                $identifier
            );
            $this->onFlashmessage('form.gallery.edit-photo.flash.success.create', Flash::SUCCESS);
        }

        //if ($form->isSubmitted()->name === 'sendBack') {
        $this->processOnBack($photo->getGallery()->getId());
        //}

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-photo',
            'id'      => $photo->getGallery()->getId(),
        ]);
    }

    public function processOnBack(?int $galleryId = null): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-gallery',
            'id'      => $galleryId,
        ]);
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->photo->isLoaded()) {
            return [];
        }

        return [
            'gallery'  => $this->photo->getGallery()->getId(),
            'name'     => $this->photo->getName(),
            'title'    => $this->photo->getTitle(true),
            'isActive' => $this->photo->isActive(),
            'video'    => $this->photo->isYoutubeToken() ? sprintf('https://www.youtube.com/embed/%s', $this->photo->getVideo()) : $this->photo->getVideo(),
        ];
    }
}
