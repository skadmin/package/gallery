<?php

declare(strict_types=1);

namespace Skadmin\Gallery\Components\Admin;

interface IEditPhotoFactory
{
    public function create(?int $id = null): EditPhoto;
}
