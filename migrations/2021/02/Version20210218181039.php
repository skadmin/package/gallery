<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210218181039 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs

        $this->addSql('DELETE FROM `translation` WHERE `hash` IN (
                                            "c67c0408d7966b0241909e5260acac47",
                                            "1e6558f8a10202d30e7ad0eb5227ee55",
                                            "1c2d499b0a6cfc6de359e3b8dc061b0c",
                                            "86cd3280f8437f692f1d5bd44ad7140e",
                                            "2f98d7ae525a84812676cf66c7eafc05",
                                            "6111be4fb6b5e79a28a487519abc5a39",
                                            "d76b49627f11ac08640c3b7e9dba8588",
                                            "d679ec2427939a0154e9e3d40b2358a9",
                                            "c15c49c37e4b9e680a337b9aa298793e",
                                            "8822d324085d97ac0b81b6ff4ed1cacf",
                                            "4067dbd4f38061567d30021b414e013b",
                                            "e1378ea2d71424c17dce180cbe79739c",
                                            "a2221c501fc1a78ed9035b1e81121389",
                                            "3aaf2297dc462632f05430aad6d2c181",
                                            "55bd0bbf8131d9bb9268389c6edac456",
                                            "1c7ba47bebba034be7622e2be3ef0bd8",
                                            "db8928f09c152d810891dbd3ebe39847"
                                          )');

        $translations = [
            ['original' => 'gallery.overview-gallery', 'hash' => 'f15d31e53d151a5910ef547321004ee9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Galerie', 'plural1' => '', 'plural2' => ''],
            ['original' => 'gallery.overview.title', 'hash' => '1e6558f8a10202d30e7ad0eb5227ee55', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Galerie|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-gallery.current-gallery', 'hash' => 'c460b7a16a4d475f7bca90dfff769a0b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přehled základních galerií', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-gallery.galleries', 'hash' => 'e56837016baec8556ee6bd484899eb0d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Galerie', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-gallery.remove-gallery - %s', 'hash' => '3e64cb92f3557b7142f23ed6fe15fc84', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Opravdu chcete odebrat galerii "%s"?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.overview-gallery.flash.success.update-gallery-sequence', 'hash' => 'e7a2845b84f6090af0c1eb7c7435f8d9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pořadí galerií bylo upraveno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'gallery.overview.title - %s', 'hash' => '39929c04b2337c8c0b015acdc93537a5', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Galerie', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-gallery.edit-current', 'hash' => 'd569e3ac168dd12588627e6478501907', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Upravit galerie', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-gallery.photos', 'hash' => '3cf46e2a3286e00f17391162d9d65184', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Fotografie', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-gallery.title', 'hash' => '0be4941d08cd2b5504a97c9939f4c566', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení galerie', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-gallery.name', 'hash' => '065faa7d1ca3ed1d7211aac4d165c527', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-gallery.name.req', 'hash' => '30d79b572b33d718f608560f28df1505', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-gallery.content', 'hash' => '5b2cd2dce96031c4ecc3c45f6fe7e0ac', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-gallery.image-preview', 'hash' => '7bb93bb3c1b5c050b1647fdd1cbf2701', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Náhled galerie', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-gallery.image-preview.rule-image', 'hash' => '93372f5c19e89fd05807420d8810e425', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-gallery.is-active', 'hash' => '6355cb552b21a1be1e5d3ce5bc407171', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-gallery.send', 'hash' => '8d6815052edbb23307bf8f2dd4a4e5b9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-gallery.back', 'hash' => '659cc5a6559a8f7bf4c9ed23586bf8ee', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zavřít', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-gallery.gallery', 'hash' => '6a4fc139252aae84b04b86785b8b62e5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Nadřazená galerie', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-gallery.title - %s', 'hash' => '77f162021d95dfdcfe2bc64a3c5bed37', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace galerie', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-gallery.remove-photo - %s', 'hash' => '82f206b0660e5a4261d10b696a5af443', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Opravdu chcete odebrat fotografii "%s"?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-photo.title - %s', 'hash' => 'a9787304b99c900f6ad4938908f77c26', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace fotografie', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-photo.name', 'hash' => '472e86d60611d4d0dbfd6b1f2cc115c8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-photo.name.req', 'hash' => '9483e0511cad8b47706e14332ba940a6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-photo.gallery', 'hash' => 'b5fd30551807800200f5d93140032282', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Galerie', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-photo.title', 'hash' => '2ec0e42e6d71ff7da78a9e907b0b8c6e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Titulek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-photo.image-preview', 'hash' => '2f221a0b830d98acaab08e1a635582ae', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Náhled fotografie', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-photo.image-preview.rule-image', 'hash' => '8c0fc81ea0180efdc1ad0f1519078933', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-photo.is-active', 'hash' => '6777c321d3ea4ded2f3593a522b43eb0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-photo.send', 'hash' => '946e33196b055fc3f9c8d76cf6bdf06e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-photo.back', 'hash' => '1380efdcca109c48c61a72d74c3df12b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zavřít', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.overview-gallery.flash.success.update-photo-sequence', 'hash' => '8f7a21703a23e8da9d25289f98da113b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pořadí fotografií bylo upraveno.', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
