<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210311180052 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'form.gallery.edit-gallery.flash.success.create', 'hash' => 'be9eacec51f1825a2d2f3113d3890c0e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Galerie byla úspěšně založena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-photo.flash.success.update', 'hash' => 'd89a18ef8dfb4196b6a98158ff639047', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Fotografie byla úspěšně upravena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.gallery.edit-gallery.flash.success.update', 'hash' => '1db3b6a8e08c8185a95d2909a43f3430', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Galerie byla úspěšně upravena.', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
