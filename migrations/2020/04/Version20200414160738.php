<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200414160738 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'form.dropzone.photos', 'hash' => '17ce1f42af418979978da44adec7f81d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'fotografie můžete přetáhnout sem', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.gallery.title', 'hash' => '3f0b17649b1a531895465c0d03092bbd', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Galerie', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.gallery.description', 'hash' => 'ae3750232dc8d079004e8a3e90d5145b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat galerie a fotografie', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
    }
}
